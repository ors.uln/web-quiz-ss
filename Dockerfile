FROM python:3.10
ENV PYTHONUNBUFFERED=1
WORKDIR /quiz
COPY quiz/requirements.txt /quiz/
RUN pip install --upgrade pip && pip install -r requirements.txt
COPY quiz /quiz