# Приложение web-quiz-ss

### Команды Docker'а
Чтобы поднять заготовку приложения
```sh
docker-compose up -d
```

Чтобы выполнить тесты
```sh
docker-compose exec web python manage.py test
```

Чтобы проверить стиль кода
```sh
docker-compose exec web pycodestyle .
```