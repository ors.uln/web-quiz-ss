from django import forms
from .dto import quiz_dto, answers_dto


CHOICES_F1 = (
    ('1-1-1', '7'),
    ('1-1-2', '8'),
    ('1-1-3', '1024'),
    ('1-1-4', '256')
)

CHOICES_F2 = (
    ('1-2-1', 'Питон'),
    ('1-2-2', 'Носорог'),
    ('1-2-3', 'Бегемот'),
    ('1-2-4', 'Крокодил')
)

CHOICES_F3 = (
    ('1-3-1', 'Windows'),
    ('1-3-2', 'Tables'),
    ('1-3-3', 'Ski'),
    ('1-3-4', 'Clouds')
)

CHOICES_F4 = (
    ('1-4-1', 'Неисчислимый'),
    ('1-4-2', 'Квантовый'),
    ('1-4-3', 'Строковый'),
    ('1-4-4', 'Булевый')
)

CHOICES_F5 = (
    ('1-5-1', 'Класс'),
    ('1-5-2', 'Школа'),
    ('1-5-3', 'Метод'),
    ('1-5-4', 'Эволюция')
)

custom_messages = {'required': 'Выберите вариант ответа'}


class RadioButtonsForm1(forms.Form):
    radio_choice_field_1 = forms.ChoiceField(
        required=True,
        label=quiz_dto.questions[0].text,
        widget=forms.RadioSelect,
        choices=CHOICES_F1,
        error_messages=custom_messages,
        initial=answers_dto.answers[0].choices
    )


class RadioButtonsForm2(forms.Form):
    radio_choice_field_2 = forms.ChoiceField(
        required=True,
        label=quiz_dto.questions[1].text,
        widget=forms.RadioSelect,
        choices=CHOICES_F2,
        error_messages=custom_messages,
        initial=answers_dto.answers[1].choices
    )


class RadioButtonsForm3(forms.Form):
    radio_choice_field_3 = forms.ChoiceField(
        required=True,
        label=quiz_dto.questions[2].text,
        widget=forms.RadioSelect,
        choices=CHOICES_F3,
        error_messages=custom_messages,
        initial=answers_dto.answers[2].choices
    )


class CheckBoxesForm4(forms.Form):
    check_boxes_field_4 = forms.MultipleChoiceField(
        required=True,
        label=quiz_dto.questions[3].text,
        widget=forms.CheckboxSelectMultiple,
        choices=CHOICES_F4,
        error_messages=custom_messages,
        initial=answers_dto.answers[3].choices
    )


class CheckBoxesForm5(forms.Form):
    check_boxes_field_5 = forms.MultipleChoiceField(
        required=True,
        label=quiz_dto.questions[4].text,
        widget=forms.CheckboxSelectMultiple,
        choices=CHOICES_F5,
        error_messages=custom_messages,
        initial=answers_dto.answers[4].choices
    )
