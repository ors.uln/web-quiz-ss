from django.test import TestCase
from typing import List
from .services import QuizResultService
from .dto import ChoiceDTO, QuestionDTO, QuizDTO, AnswerDTO, AnswersDTO


class BaseTestCase(TestCase):
    def setUp(self):
        choices: List[ChoiceDTO] = [
            ChoiceDTO('1-5-1', 'Класс', True),
            ChoiceDTO('1-5-2', 'Школа', False),
            ChoiceDTO('1-5-3', 'Метод', True),
            ChoiceDTO('1-5-4', 'Эволюция', False)
        ]

        questions: List[QuestionDTO] = [
            QuestionDTO('1-5',
                        'Термины ООП.',
                        choices)
        ]

        self.quiz_dto = QuizDTO(
            '1',
            'Programming.',
            questions
        )

    def test_success_quiz_result(self):
        answers: List[AnswerDTO] = [
            AnswerDTO(
                '1-5',
                ['1-5-1', '1-5-3']
            )
        ]

        answers_dto = AnswersDTO(
            '1',
            answers
        )

        quiz_result_service = QuizResultService(
            self.quiz_dto,
            answers_dto
        )

        self.assertEqual(quiz_result_service.get_result(), 0.20)

    def test_failure_quiz_result(self):
        answers: List[AnswerDTO] = [
            AnswerDTO(
                '1-5',
                ['1-5-1', '1-5-3', '1-5-4']
            )
        ]

        answers_dto = AnswersDTO(
            '1',
            answers
        )

        quiz_result_service = QuizResultService(
            self.quiz_dto,
            answers_dto
        )

        self.assertEqual(quiz_result_service.get_result(), 0.00)
