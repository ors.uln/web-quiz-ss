from .dto import QuizDTO, AnswersDTO


class QuizResultService():
    def __init__(self, quiz_dto: QuizDTO, answers_dto: AnswersDTO):
        self.quiz_dto = quiz_dto
        self.answers_dto = answers_dto

    def get_result(self) -> float:
        quiz_mark = 0
        for i, k in zip(self.quiz_dto.questions, self.answers_dto.answers):
            quest_mark = 1
            for j in i.choices:
                if (j.is_correct and j.uuid in k.choices) or \
                        (not j.is_correct and j.uuid not in k.choices):
                    pass
                else:
                    quest_mark = 0
                    break
            quiz_mark += quest_mark
        return quiz_mark/5
