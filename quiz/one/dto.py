from typing import List, NamedTuple


class ChoiceDTO(NamedTuple):
    uuid: str
    text: str
    is_correct: bool


class QuestionDTO(NamedTuple):
    uuid: str
    text: str
    choices: List[ChoiceDTO]


class QuizDTO(NamedTuple):
    uuid: str
    title: str
    questions: List[QuestionDTO]


class AnswerDTO(NamedTuple):
    question_uuid: str
    choices: List[str]


class AnswersDTO(NamedTuple):
    quiz_uuid: str
    answers: List[AnswerDTO]


quiz_dto = QuizDTO(
    '1',
    'Programming.',
    [
        QuestionDTO(
            '1-1',
            'Бит в одном байте.',
            [
                ChoiceDTO(
                    '1-1-1',
                    '7',
                    False
                ),
                ChoiceDTO(
                    '1-1-2',
                    '8',
                    True
                ),
                ChoiceDTO(
                    '1-1-3',
                    '1024',
                    False
                ),
                ChoiceDTO(
                    '1-1-4',
                    '256',
                    False
                )
            ]
        ),
        QuestionDTO(
            '1-2',
            'Язык программирования.',
            [
                ChoiceDTO(
                    '1-2-1',
                    'Питон',
                    True
                ),
                ChoiceDTO(
                    '1-2-2',
                    'Носорог',
                    False
                ),
                ChoiceDTO(
                    '1-2-3',
                    'Бегемот',
                    False
                ),
                ChoiceDTO(
                    '1-2-4',
                    'Крокодил',
                    False
                )
            ]
        ),
        QuestionDTO(
            '1-3',
            'Операционная система.',
            [
                ChoiceDTO(
                    '1-3-1',
                    'Windows',
                    True
                ),
                ChoiceDTO(
                    '1-3-2',
                    'Tables',
                    False
                ),
                ChoiceDTO(
                    '1-3-3',
                    'Ski',
                    False
                ),
                ChoiceDTO(
                    '1-3-4',
                    'Clouds',
                    False
                )
            ]
        ),
        QuestionDTO(
            '1-4',
            'Типы данных.',
            [
                ChoiceDTO(
                    '1-4-1',
                    'Неисчислимый',
                    False
                ),
                ChoiceDTO(
                    '1-4-2',
                    'Квантовый',
                    False
                ),
                ChoiceDTO(
                    '1-4-3',
                    'Строковый',
                    True
                ),
                ChoiceDTO(
                    '1-4-4',
                    'Булевый',
                    True
                )
            ]
        ),
        QuestionDTO(
            '1-5',
            'Термины ООП.',
            [
                ChoiceDTO(
                    '1-5-1',
                    'Класс',
                    True
                ),
                ChoiceDTO(
                    '1-5-2',
                    'Школа',
                    False
                ),
                ChoiceDTO(
                    '1-5-3',
                    'Метод',
                    True
                ),
                ChoiceDTO(
                    '1-5-4',
                    'Эволюция',
                    False
                )
            ]
        )
    ]
)


answers_dto = AnswersDTO(
    '1',
    [
        AnswerDTO(
            '1-1',
            []
        ),
        AnswerDTO(
            '1-2',
            []
        ),
        AnswerDTO(
            '1-3',
            []
        ),
        AnswerDTO(
            '1-4',
            []
        ),
        AnswerDTO(
            '1-5',
            []
        )
    ]
)
