from django.shortcuts import render
from .forms import RadioButtonsForm1, RadioButtonsForm2, RadioButtonsForm3,\
    CheckBoxesForm4, CheckBoxesForm5
from django.http import HttpResponseRedirect
from .services import QuizResultService
from .dto import quiz_dto, answers_dto


def page_0(request):
    for y in range(5):
        answers_dto.answers[y].choices.clear()
    return render(request, "page_0.html")


def page_1(request, q=0):
    radio_buttons_form_1 = RadioButtonsForm1(request.POST or None)
    radio_buttons_form_2 = RadioButtonsForm2(request.POST or None)
    radio_buttons_form_3 = RadioButtonsForm3(request.POST or None)
    check_boxes_form_4 = CheckBoxesForm4(request.POST or None)
    check_boxes_form_5 = CheckBoxesForm5(request.POST or None)
    form_list = [
        radio_buttons_form_1,
        radio_buttons_form_2,
        radio_buttons_form_3,
        check_boxes_form_4,
        check_boxes_form_5
    ]
    choice_list = [
        'radio_choice_field_1',
        'radio_choice_field_2',
        'radio_choice_field_3',
        'check_boxes_field_4',
        'check_boxes_field_5'
    ]
    if request.POST:
        answers_dto.answers[q].choices.clear()
        if form_list[q].is_valid():
            if q in range(3):
                answers_dto.answers[q].choices.append(form_list[q].cleaned_data[choice_list[q]])
            else:
                answers_dto.answers[q].choices.extend(form_list[q].cleaned_data[choice_list[q]])
        else:
            return render(request, "page_1.html", {'form': form_list[q], 'number': q+1})
    else:
        return render(request, "page_1.html", {'form': form_list[q], 'number': q+1})
    directory = q + 2
    return HttpResponseRedirect(str(directory))


def page_2(request, q=1):
    radio_buttons_form_1 = RadioButtonsForm1(request.POST or None)
    radio_buttons_form_2 = RadioButtonsForm2(request.POST or None)
    radio_buttons_form_3 = RadioButtonsForm3(request.POST or None)
    check_boxes_form_4 = CheckBoxesForm4(request.POST or None)
    check_boxes_form_5 = CheckBoxesForm5(request.POST or None)
    form_list = [
        radio_buttons_form_1,
        radio_buttons_form_2,
        radio_buttons_form_3,
        check_boxes_form_4,
        check_boxes_form_5
    ]
    choice_list = [
        'radio_choice_field_1',
        'radio_choice_field_2',
        'radio_choice_field_3',
        'check_boxes_field_4',
        'check_boxes_field_5'
    ]
    if request.POST:
        answers_dto.answers[q].choices.clear()
        if form_list[q].is_valid():
            if q in range(3):
                answers_dto.answers[q].choices.append(form_list[q].cleaned_data[choice_list[q]])
            else:
                answers_dto.answers[q].choices.extend(form_list[q].cleaned_data[choice_list[q]])
        else:
            return render(request, "page_1.html", {'form': form_list[q], 'number': q+1, 'page_back': '1'})
    else:
        return render(request, "page_1.html", {'form': form_list[q], 'number': q+1, 'page_back': '1'})
    directory = q + 2
    return HttpResponseRedirect(str(directory))


def page_3(request, q=2):
    radio_buttons_form_1 = RadioButtonsForm1(request.POST or None)
    radio_buttons_form_2 = RadioButtonsForm2(request.POST or None)
    radio_buttons_form_3 = RadioButtonsForm3(request.POST or None)
    check_boxes_form_4 = CheckBoxesForm4(request.POST or None)
    check_boxes_form_5 = CheckBoxesForm5(request.POST or None)
    form_list = [
        radio_buttons_form_1,
        radio_buttons_form_2,
        radio_buttons_form_3,
        check_boxes_form_4,
        check_boxes_form_5
    ]
    choice_list = [
        'radio_choice_field_1',
        'radio_choice_field_2',
        'radio_choice_field_3',
        'check_boxes_field_4',
        'check_boxes_field_5'
    ]
    if request.POST:
        answers_dto.answers[q].choices.clear()
        if form_list[q].is_valid():
            if q in range(3):
                answers_dto.answers[q].choices.append(form_list[q].cleaned_data[choice_list[q]])
            else:
                answers_dto.answers[q].choices.extend(form_list[q].cleaned_data[choice_list[q]])
        else:
            return render(request, "page_1.html", {'form': form_list[q], 'number': q+1, 'page_back': '2'})
    else:
        return render(request, "page_1.html", {'form': form_list[q], 'number': q+1, 'page_back': '2'})
    directory = q + 2
    return HttpResponseRedirect(str(directory))


def page_4(request, q=3):
    radio_buttons_form_1 = RadioButtonsForm1(request.POST or None)
    radio_buttons_form_2 = RadioButtonsForm2(request.POST or None)
    radio_buttons_form_3 = RadioButtonsForm3(request.POST or None)
    check_boxes_form_4 = CheckBoxesForm4(request.POST or None)
    check_boxes_form_5 = CheckBoxesForm5(request.POST or None)
    form_list = [
        radio_buttons_form_1,
        radio_buttons_form_2,
        radio_buttons_form_3,
        check_boxes_form_4,
        check_boxes_form_5
    ]
    choice_list = [
        'radio_choice_field_1',
        'radio_choice_field_2',
        'radio_choice_field_3',
        'check_boxes_field_4',
        'check_boxes_field_5'
    ]
    if request.POST:
        answers_dto.answers[q].choices.clear()
        if form_list[q].is_valid():
            if q in range(3):
                answers_dto.answers[q].choices.append(form_list[q].cleaned_data[choice_list[q]])
            else:
                answers_dto.answers[q].choices.extend(form_list[q].cleaned_data[choice_list[q]])
        else:
            return render(request, "page_1.html", {'form': form_list[q], 'number': q+1, 'page_back': '3'})
    else:
        return render(request, "page_1.html", {'form': form_list[q], 'number': q+1, 'page_back': '3'})
    directory = q + 2
    return HttpResponseRedirect(str(directory))


def page_5(request, q=4):
    radio_buttons_form_1 = RadioButtonsForm1(request.POST or None)
    radio_buttons_form_2 = RadioButtonsForm2(request.POST or None)
    radio_buttons_form_3 = RadioButtonsForm3(request.POST or None)
    check_boxes_form_4 = CheckBoxesForm4(request.POST or None)
    check_boxes_form_5 = CheckBoxesForm5(request.POST or None)
    form_list = [
        radio_buttons_form_1,
        radio_buttons_form_2,
        radio_buttons_form_3,
        check_boxes_form_4,
        check_boxes_form_5
    ]
    choice_list = [
        'radio_choice_field_1',
        'radio_choice_field_2',
        'radio_choice_field_3',
        'check_boxes_field_4',
        'check_boxes_field_5'
    ]
    if request.POST:
        answers_dto.answers[q].choices.clear()
        if form_list[q].is_valid():
            if q in range(3):
                answers_dto.answers[q].choices.append(form_list[q].cleaned_data[choice_list[q]])
            else:
                answers_dto.answers[q].choices.extend(form_list[q].cleaned_data[choice_list[q]])
        else:
            return render(request, "page_1.html", {'form': form_list[q], 'number': q+1, 'page_back': '4'})
    else:
        return render(request, "page_1.html", {'form': form_list[q], 'number': q+1, 'page_back': '4'})
    return HttpResponseRedirect('result')


def result(request):
    quiz_result_service = QuizResultService(quiz_dto, answers_dto)
    quiz_result = f'{quiz_result_service.get_result():1.2f}'
    return render(request, "page_2.html", {'result': quiz_result})
